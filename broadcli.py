import socket

connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
connection.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

connection.bind(("", 54345))
while True:
    print(connection.recvfrom(1024)[0])