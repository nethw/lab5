import socket
import time
from datetime import datetime

connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
connection.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

while True:
    time.sleep(1)
    connection.sendto(str(datetime.now()).encode('ascii'), ("", 54345))