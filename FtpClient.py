import argparse
import os
import sys
from ftplib import FTP

ftp = FTP()
ftp.connect("127.0.0.1")
ftp.login('TestUser', '')

#ls - 2nd param is dir path
#inc = ['ls', './']
#send - 2nd param is youy local file path, 3rd param is path where it will appear on the server
#inc = ['send', 
#       'C:/Users/MSI GL75/huawei_2022_autumn/Multi-Grid-Deep-Homography/Codes/utils.py', 
#       './CLionProjects/nethwi4.txt']
#recieve - 2nd param is server file path, 3rd param is path where file will apear on client
inc = ['recieve',
       './CLionProjects/nethwi1.txt',
       'C:/Users/MSI GL75/huawei_2022_autumn/Multi-Grid-Deep-Homography/Codes/utils8.py']

if inc[0] == 'ls':
    ftp.dir(inc[1])
elif inc[0] == 'send':
    ftp.storbinary(f"STOR {inc[2]}", open(inc[1], "rb"))
elif inc[0] == 'recieve':
    ftp.retrbinary(f"RETR {inc[1]}", open(inc[2], "wb").write)
else:
    print("error, this command can not be recognized")
