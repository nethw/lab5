import socket
import subprocess

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('localhost', 54343))
server.listen(5)

while True:
    connection = server.accept()[0]

    if connection.recv(1024) == bytes("ping", encoding='UTF-8'):
        result = subprocess.check_output(["ping", "yandex.ru", "-c", "1"])

    connection.send(result)
    connection.close()